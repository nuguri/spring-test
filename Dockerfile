FROM maven:3.6.3-jdk-11 AS builder
WORKDIR /workspace/docker-source
RUN mvn clean verify

FROM openjdk:11
WORKDIR /app
COPY --from=builder /workspace/docker-source/target/spring-0.0.1-SNAPSHOT.war .
CMD ["java", "-jar", "spring-0.0.1-SNAPSHOT.war"]